import { Component, OnInit, Input } from '@angular/core';
import { PaymentService } from '../payment.service';

@Component({
  selector: 'payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  @Input() data: any;
  name;
  amount;
  key;
  showButton = false;

  constructor(protected paymentService: PaymentService) { }

  ngOnInit() {
    this.name = this.data.name;
    this.amount = this.data.amount;
    this.key = this.data.$key;
  }

  delete() {
    this.paymentService.deletePayment(this.key);
  }

  buttonOn() {
    this.showButton = true;
  }

  buttonOff() {
    this.showButton = false;
  }
}
