import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { PaymentService } from '../payment.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent implements OnInit {
  name: string;
  amount: string;
  code: string;
  building: string;
  uid: string;
  show = false;
  payments = [];
  // displayedColumns: string[] = ['Payer', 'Amount'];

  constructor(public authService:AuthService, protected db: AngularFireDatabase, protected paymentService:PaymentService) { }

  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        if (!user) return;
        this.uid = user.uid;
        this.db.list('/users/'+user.uid+'/details').snapshotChanges().subscribe(
          data => {
            data.forEach(
              dat => {
                let y = dat.payload.toJSON();
                this.building = JSON.stringify(y).slice(1,-1);
              }
            )
          }
        )
        this.db.list('/users/'+user.uid+'/payments').snapshotChanges().subscribe(
          payments => {
            this.payments = [];
            payments.forEach(
              payment => {
                let y = payment.payload.toJSON();
                y['$key'] = payment.key;
                this.payments.push(y);
              }
            )
          }
        )
      }
    )
  }

  pay() {
    this.paymentService.addPayment(this.name, this.amount, this.code);
    this.show = true;
    this.name = "";
    this.amount = "";
    this.code = "";
  }
}
