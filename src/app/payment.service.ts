import { Injectable } from '@angular/core';
import { AngularFireDatabase, snapshotChanges } from '@angular/fire/database';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(protected db:AngularFireDatabase, protected authService: AuthService) { }

  addPayment(name: string, amount: string, code: string) {
    this.db.list('/users/'+code+'/payments').push({'name': name, 'amount': amount});
  }

  deletePayment(key: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/payments').remove(key);
      }
    )
  }
}
