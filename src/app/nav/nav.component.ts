import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(protected router:Router, public authService: AuthService) { }

  ngOnInit() {
  }

  toLogin() {
    this.router.navigate(['/login']);
  }
  
  toPayments() {
    this.router.navigate(['/payments']);
  }

  logOut() {
    this.authService.logOut()
    .then(value => {
      this.router.navigate(['/payments'])
    });
  }

}